# PPM_Image - Description

This Java application gives the opportunity to user can open and read an image file in ppm format and process it with certain types of filters. 
The supported filters are the gray, color, blur, diff and median filters.
Finally, the user can save any changes, which has made in a file .ppm. 
Additionally, all of these functions are provided to the user with the help of a Graphical User Interface (GUI) for the application, created with the Swing library.

The applied Filters
-
More specific: 

- Gray filter: The application of this filter converts the image into grayscale.
- Color filter: With this filter, each one of the pixel's color channel value (red,green,blue) is multiplied by a specific factor.
- Blur filter: With this filter, each pixel takes the average value of it's neighbours.
- Diff filter: Each pixel takes the difference between it's maximum and it's minimum value of neighbours' color.
- Median filter: Each pixel takes the median value of it's neighbours.



Installation
-
In order to clone the repository open a command line and issue:

`git clone https://EleniMangiorou@bitbucket.org/EleniMangiorou/ppm_image.git`


In eclipse, `File > Import > Git > Projects from Git > Existing Local Repository`. 
In the Import Projects from Git screen, click `add`, then goto the location of the cloned repository. 
Click `open`, `finish`, then `next`. Select `Java > Java Project`, and click `next` again.

At this point, there are three options.

* Import existing projects
* Use the New Project Wizard 
* Import as General Project 


Pick  `Use the New Project Wizard`. 
Then, uncheck `Use Default location`, and click `browse`. 
Select a folder (You probably want the location you have cloned the repository to), click `Open`, and give the Project a name. 
Click `next`, make sure that the src folders are setup correctly, and click `finish`. 



Build and Run the project
-

There is a shortcut icon in the Eclipse toolbar for running the current file as an application.  
Click the `FilterGUI.java` and then click the `play` icon. 
The GUI of filters will start.

Contact
-

e-mail: mangiorou.eleni@gmail.com