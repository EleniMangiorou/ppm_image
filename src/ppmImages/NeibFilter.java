package ppmImages;

import java.util.ArrayList;

public class NeibFilter extends Filter{


	Image tempimage;

	ArrayList<Integer> redList = new ArrayList<Integer>();
	ArrayList<Integer> greenList = new ArrayList<Integer>();
	ArrayList<Integer> blueList = new ArrayList<Integer>();

	// Creates and keeps a copy of the original image,
	// because the image, in which we write should be different from what we read.

	public void copyImage(Image image){
		int width = image.getWidth();
		int height = image.getHeight();
		Color [] imgbuffer = image.getBuffer();
		tempimage = new Image(width, height, imgbuffer);
	}

	public Color FilterFunction(Image image, int x, int y){

		redList.clear();
		greenList.clear();
		blueList.clear();

		for (int i = (x - 1); i <= (x + 1); i++){
			for (int j = (y - 1); j <= (y + 1); j++){
				if ((i >= 0) && (i <image.getWidth()) && (j >= 0) && (j < image.getHeight())){
					collectNeibColors(i, j);
				}
			}
		}
		Color u = new Color();
		u = neibFilterFunction();
		return u;
	}

	// Reads and keeps in an Arraylist the red, green and blue components 
	// of the colors in the neighborhood of a pixel  
	// and separately for each color channel.
	public void collectNeibColors(int i, int j){
		redList.add((tempimage.getPixel(i, j)).getRed());
		greenList.add((tempimage.getPixel(i, j)).getGreen());
		blueList.add((tempimage.getPixel(i, j)).getBlue());;
	}


	public Color neibFilterFunction(){
		Color u = new Color();
		return u;
	}


}
