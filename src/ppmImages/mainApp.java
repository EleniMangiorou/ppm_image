package ppmImages;

import java.io.IOException;
import java.util.regex.Pattern;

public class mainApp {

	public static void main(String[] args) throws IOException {

		Image img = new Image(12, 3, 255);

		System.out.println("Width : " + img.getWidth() + "\n" + "Heigth : " + img.getHeight() 
		+ "\n" + "MaxColor : " + img.getMaxColor());

		Color [] newBuffer = new Color[4];
		Color c = new Color(20, 50, 255);
		Color c1 = new Color(40, 50, 255);
		newBuffer[0]=  newBuffer[1] = c;
		newBuffer[2]=  newBuffer[3] = c1;

		Image img1 = new Image(2, 2, 255, newBuffer);

		System.out.println(img1);


		Image img2 = new Image();
		ppm_format ppmFormat = new ppm_format();

		String inFileName = "Image01.ppm";
		String[] parts = inFileName.split(Pattern.quote("."));
		String part1inFileName = parts[0];
		img2 = ppmFormat.ReadPPM(inFileName);


		Color cBefore = img2.AverageValuesofColors();
		System.out.println("Before the filter the average values of the colors of the image is: ");
		System.out.println("Red : " + cBefore.getRed() + "\nGreen : "  + cBefore.getGreen() + "\nBlue : " + cBefore.getBlue() + "\n" );


		// Applies the Gray Filter into a ppm image
		// GrayFilter grayFilter = new GrayFilter();
		// img2 = grayFilter.applyFilter(img2);

		// Applies the Color Filter into a ppm image
		// ColorFilter colorFilter = new ColorFilter(130,3,58);
		// img2 = colorFilter.applyFilter(img2);

		// Applies the Diff Filter into a ppm image
		// DiffFilter diffFilter = new DiffFilter();
		// img2 = diffFilter.applyFilter(img2);

		//Applies the Median Filter into a ppm image
		// MedianFilter medianFilter = new MedianFilter();
		// img2 = medianFilter.applyFilter(img2);

		//Applies the Blur Filter into a ppm image
		BlurFilter blurFilter = new BlurFilter();
		img2 = blurFilter.applyFilter(img2);

		Color cAfter = img2.AverageValuesofColors();
		System.out.println("After the filter the average values of the colors of the image is: ");
		System.out.println("Red : " + cAfter.getRed() + "\nGreen : "  + cAfter.getGreen() + "\nBlue : " + cAfter.getBlue() + "\n" );

		String outFileName = part1inFileName  +  ".filtered";
		ppmFormat.WritePPM(img2, outFileName);

	}

}
