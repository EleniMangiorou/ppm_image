package ppmImages;

public class Color {
	private int red;
	private int green;
	private int blue;

	// Constructors
	public Color() {
		this.red = 0;
		this.green = 0;
		this.blue = 0;
	}

	public Color(int r, int g, int b) {
		this.red = r;
		this.green = g;
		this.blue = b;
	}

	public Color(Color c) {
		this.red = c.getRed();
		this.green = c.getGreen();
		this.blue = c.getBlue();
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		this.red = red;
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		this.green = green;
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		this.blue = blue;
	}

	public void setColor(int red, int green, int blue){
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public String toString(){
		return "\nColor \nRed : " + red + "\nGreen : " + green + "\nBlue : " + blue + "\n";
	}


}
