package ppmImages;

public class ColorFilter  extends Filter{

	// It is a local filter 
	
	int red = 0;
	int green = 0;
	int blue = 0;

	ColorFilter(int Red, int Green, int Blue) {
		this.red = Red;
		this.green = Green;
		this.blue = Blue;
	}


	// Multiplies a specific color to the color of the pixel in the location (x,y)
	// and returns the final color of this pixel.
	public Color FilterFunction(Image image, int x, int y){

		Color c = new Color(red, green, blue);
		Color p = (image.getPixel(x, y));
		Color s = new Color(((c.getRed()*p.getRed())/255), ((c.getGreen()*p.getGreen())/255), ((c.getBlue()*p.getBlue())/255));
		return s;
	}


}

