package ppmImages;

public class Filter {


	public Image applyFilter(Image image){
		
		// Creates a copy of original image
		copyImage(image);
		
		for (int x = 0; x <  image.getWidth(); x++){
			for ( int y = 0; y < image.getHeight(); y++){
				Color s = FilterFunction(image, x, y);
				image.setPixel(x, y, s);
			}
		}
		return image;
	}


	public Color FilterFunction(Image image, int x, int y) {
		Color t = new Color(0, 0, 0);
		return t;
	}

	public void copyImage(Image image){

		System.out.println(" This method creates a copy of the original image. ");
	}
	
	
}
