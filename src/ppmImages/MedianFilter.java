package ppmImages;

import java.util.Collections;

public class MedianFilter extends NeibFilter{

	// It's a neighborhood filter
	// The median filter selects and returns the median value 
	// of a specific color (red, green and blue) by the corresponding sorted Arraylist.
	// This filter reduces the noise from an image.

	public Color neibFilterFunction(){

		Collections.sort(redList);
		Collections.sort(greenList);
		Collections.sort(blueList);

		int k = redList.size();
		Color c = new Color(redList.get(k/2), greenList.get(k/2), blueList.get(k/2));
		return c;
	}

}
