package ppmImages;

public class GrayFilter extends Filter{

	
	//It's a local filter
	
	//Converts the color of a pixel into grayscale color
	//and returns this color.

	public Color FilterFunction(Image image,  int x,  int y){

		// For each pixel
		Color temp = image.getPixel(x, y);
		int m = (temp.getRed() + temp.getGreen() + temp.getBlue()) / 3;
		Color t =  new Color(m, m, m);
		return t;
	}

}
