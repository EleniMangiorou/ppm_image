package ppmImages;

public class BlurFilter extends NeibFilter {
	
	// It's a neighborhood filter
	// The Blur filter calculates and returns the average color 
	// of a pixel by the colors of the corresponding neighboring pixels. 
	
	public Color neibFilterFunction(){
		
		int sumRedColor = 0; 
		int sumGreenColor = 0; 
		int sumBlueColor = 0; 
		
		for (Integer redColor:redList)
			sumRedColor = sumRedColor + redColor;
		for (Integer greenColor:greenList)
			sumGreenColor = sumGreenColor + greenColor;
		for (Integer blueColor:blueList)
			sumBlueColor = sumBlueColor + blueColor;
		
		int k = redList.size();
		Color u = new Color(sumRedColor/k, sumGreenColor/k, sumBlueColor/k);
		return u;

	}
	
	
	
}
