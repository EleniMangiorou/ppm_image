package ppmImages;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;


public class FilterGUI extends JFrame implements ActionListener {


	private static final long serialVersionUID = 8828888437716140055L;

	private JPanel pane1;

	private JButton buttonLoad;
	private JButton buttonSave;
	private JButton buttonGrayFilter;
	private JButton buttonColorFilter;
	private JButton buttonBlurFilter;
	private JButton buttonDiffFilter;
	private JButton buttonMedianFilter;
	private JButton buttonAverageColors;
	private JButton buttonUndo;
	private JButton buttonRedo;

	private JLabel historyLabel; 
	private JLabel filterLabel;
	private JLabel AverageColorsLabel; 
	private JLabel redLabel; 
	private JLabel greenLabel; 
	private JLabel blueLabel; 
	private JLabel redDataLabel; 
	private JLabel greenDataLabel; 
	private JLabel blueDataLabel; 
	private JLabel scaleLabel;

	private JFileChooser fc;

	private JTextPane textPane;
	private JTextArea tArea;

	private JScrollPane areaScrollPane1;
	private JScrollPane areaScrollPane2;

	private ppm_format loadSave = new ppm_format();

	private GrayFilter grayFilter;
	private ColorFilter colorFilter;
	private BlurFilter blurFilter;
	private DiffFilter diffFilter;
	private MedianFilter medianFilter;

	private ColorFilterDialog colorFilterDialog;
	private ColorFilterDialogData colorFilterDialogData;

	private ArrayList<Image> arrayListImage = new ArrayList<Image>();
	private ArrayList<Image> arrayListRedo = new ArrayList<Image>();


	//Constructor of frame
	public FilterGUI() {
		super("Filter GUI");
	}

	public static void main(String[] args) {
		//Created the frame
		FilterGUI frame = new FilterGUI();
		frame.drawFrame();
	}// main

	private void drawFrame() {
		JFrame frame = new JFrame();
		frame.setTitle("Image Filtering Application");
		frame.setBounds(50, 50, 800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(true);

		// ***************** panel's components ***************** //

		fc = new JFileChooser();

		buttonLoad = new JButton("Load File...");
		buttonSave= new JButton("Save File...");
		buttonGrayFilter = new JButton("Gray filter");		
		buttonColorFilter= new JButton("Color filter");
		buttonBlurFilter = new JButton("Blur filter");
		buttonDiffFilter = new JButton("Diff filter");
		buttonMedianFilter = new JButton("Median filter");
		buttonAverageColors = new JButton("Average Colors");
		buttonUndo = new JButton("Undo");
		buttonRedo = new JButton("Redo");

		filterLabel = new JLabel("  Choose a filter to apply into your image!  ");
		AverageColorsLabel = new JLabel("  The average colors of the image are: ");
		scaleLabel = new JLabel("  (The scale is 0 to 255)");
		redLabel = new JLabel("  Red:  ");
		greenLabel = new JLabel("  Green:  ");
		blueLabel = new JLabel("  Blue:  ");
		redDataLabel = new JLabel();
		greenDataLabel = new JLabel();
		blueDataLabel = new JLabel();

		buttonLoad.addActionListener(this);
		buttonSave.addActionListener(this);
		buttonGrayFilter.addActionListener(this);
		buttonColorFilter.addActionListener(this);
		buttonBlurFilter.addActionListener(this);
		buttonDiffFilter.addActionListener(this);
		buttonMedianFilter.addActionListener(this);
		buttonAverageColors.addActionListener(this);
		buttonUndo.addActionListener(this);
		buttonRedo.addActionListener(this);

		historyLabel = new JLabel("  History:  ");		

		pane1 = new JPanel();
		textPane = new JTextPane();

		buttonSave.setEnabled(false);	
		buttonUndo.setEnabled(false);
		buttonRedo.setEnabled(false);

		areaScrollPane1 = new JScrollPane(textPane);
		areaScrollPane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		areaScrollPane1.setPreferredSize(new Dimension(1000, 600));

		tArea = new JTextArea();
		tArea.setFont(new Font("Serif", Font.ITALIC, 14));
		tArea.setLineWrap(true);
		tArea.setSize(500, 250);

		areaScrollPane2 = new JScrollPane(tArea);
		areaScrollPane2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		areaScrollPane2.setPreferredSize(new Dimension(100, 100));

		pane1.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0, 0, 0, 5);
		c.weightx = 0.1;
		c.weighty = 0.1;
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 0;

		pane1.add(buttonLoad, c);

		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 0;
		pane1.add(buttonSave, c);

		c.insets = new Insets(10, 5, 5, 5);
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 1;
		pane1.add(filterLabel, c);

		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 2;
		pane1.add(buttonGrayFilter, c);

		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 3;
		pane1.add(buttonColorFilter, c);

		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 4;
		pane1.add(buttonBlurFilter, c);

		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 5;
		pane1.add(buttonDiffFilter, c);

		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 6;
		pane1.add(buttonMedianFilter, c);


		c.insets = new Insets(30, 5, 0, 5 );
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 10;
		pane1.add(buttonAverageColors, c);

		c.insets = new Insets(0, 5, 0, 5 );
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 11;
		pane1.add(AverageColorsLabel, c);
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 12;
		pane1.add(scaleLabel, c);

		c.insets = new Insets(5, 5, 0, 5 );
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 13;
		pane1.add(redLabel, c);

		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 14;
		pane1.add(greenLabel, c);

		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 15;
		pane1.add(blueLabel, c);

		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 13;
		pane1.add(redDataLabel, c);

		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 14;
		pane1.add(greenDataLabel, c);

		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 15;
		pane1.add(blueDataLabel, c);

		c.insets = new Insets(60, 5, 0, 5 );
		c.weighty = 1.0;
		c.gridheight = 1;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 16;
		pane1.add(historyLabel, c);


		c.insets = new Insets(0, 5, 0, 5 );
		c.gridheight = 3;
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 18;
		c.weightx = 0;
		c.weighty = 0;
		pane1.add(areaScrollPane2, c);

		c.insets = new Insets(0, 0, 0, 5);
		c.weightx = 0.1;
		c.weighty = 0.1;
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 21;
		pane1.add(buttonUndo, c);

		c.weightx = 0.1;
		c.weighty = 0.1;
		c.gridwidth = 1;
		c.gridx = 1;
		c.gridy = 21;
		pane1.add(buttonRedo, c);


		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, 
				true, pane1, areaScrollPane1);

		splitPane.setOneTouchExpandable(true);

		getContentPane().add(splitPane);


		frame.add(splitPane);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == buttonLoad) {

			try {
				File currentDir = new File(new File(".").getCanonicalPath());
				fc.setCurrentDirectory(currentDir);
			}
			catch (java.io.IOException e2) {
			}

			int returnVal = fc.showOpenDialog(this);

			if (returnVal == JFileChooser.APPROVE_OPTION) {

				File file = fc.getSelectedFile();

				System.out.println(file.getName());
				try {
					Image img = loadSave.ReadPPM(file.getPath());
					Image tempImage = new Image(img);
					arrayListImage.clear();
					arrayListRedo.clear();

					arrayListImage.add(tempImage);

					buttonUndo.setEnabled(false);
					buttonRedo.setEnabled(false);
					buttonSave.setEnabled(true);

					textPane.setText(""); 
					textPane.insertIcon(new ImageIcon(paintImage(img)));

					tArea.append("Loaded the file " + file.getName() +" \n");
					tArea.append("The path is : " + file.getPath() +" \n\n");
				} catch (IOException e1) {
					e1.printStackTrace();
				}

			} else {
				tArea.append("Couldn't load file...\n\n");
			}
		}
		else if (e.getSource() == buttonSave) {

			try {
				File currentDir = new File(new File(".").getCanonicalPath());
				fc.setCurrentDirectory(currentDir);
				int returnVal = fc.showSaveDialog(this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();

					System.out.println(file.getName());

					Image img = arrayListImage.get(arrayListImage.size()-1);
					loadSave.WritePPM(img, file.getPath());

					tArea.append("Saved the file " + file.getName()+ " \n");
					tArea.append("The path is : " + file.getPath() + " \n\n");

				} else {
					tArea.append("Couldn't save file...\n\n");
				}
			}
			catch (java.io.IOException e2) {
			}
		}
		else if (e.getSource() == buttonGrayFilter){

			System.out.println("Gray Filter button clicked");

			grayFilter = new GrayFilter();
			applyFilter(grayFilter);

			System.out.println("The Gray filter applied \n\n");
			tArea.append("The Gray filter applied \n\n");

		}
		else if (e.getSource() == buttonColorFilter){

			System.out.println("Color Filter button clicked");

			colorFilterDialog = new ColorFilterDialog(this, this);
			colorFilterDialog.pack();
			colorFilterDialog.setVisible(true);

			colorFilterDialogData = colorFilterDialog.getDialogColorFilterData();

			if (colorFilterDialogData != null && colorFilterDialogData.getRedColor()>0  && colorFilterDialogData.getGreenColor()>0 
					&& colorFilterDialogData.getBlueColor()>0) {
				// The text is valid.

				// Apply the Color filter
				// ToDo
				System.out.println("The red color is " + colorFilterDialogData.getRedColor() + "\nthe green color is " + colorFilterDialogData.getGreenColor() + "\nthe green color is " 
						+ colorFilterDialogData.getBlueColor() + "\n");

				colorFilter = new ColorFilter(colorFilterDialogData.getRedColor(), colorFilterDialogData.getGreenColor(), colorFilterDialogData.getBlueColor());

				applyFilter(colorFilter);

				System.out.println("The Color filter applied \n\n");
				tArea.append("The Color filter applied \n\n");

			} else {
				tArea.setText("The Color filter canceled !!! \n Invalid inputs !!! ");
			}
		}
		else if (e.getSource() == buttonBlurFilter){

			System.out.println("Blur Filter button clicked");

			blurFilter =  new BlurFilter();
			applyFilter(blurFilter);

			System.out.println("The Blur filter applied \n\n");
			tArea.append("The Blur filter applied \n\n");

		}
		else if (e.getSource() == buttonDiffFilter){

			System.out.println("Diff Filter button clicked");

			diffFilter =  new DiffFilter();
			applyFilter(diffFilter);

			System.out.println("The Diff filter applied \n\n");
			tArea.append("The Diff filter applied \n\n");

		}
		else if (e.getSource() == buttonMedianFilter){

			System.out.println("Median Filter button clicked");

			medianFilter = new MedianFilter();
			applyFilter(medianFilter);

			System.out.println("The Median filter applied \n\n");
			tArea.append("The Median filter applied \n\n");

		}
		else if (e.getSource() == buttonAverageColors){

			System.out.println("Average Colors button clicked");
			Image img = arrayListImage.get(arrayListImage.size()-1);
			Color c = img.AverageValuesofColors();

			redDataLabel.setText(" " + c.getRed() +" ");
			greenDataLabel.setText(" " + c.getGreen() +" ");
			blueDataLabel.setText(" " + c.getBlue() +" ");

		}
		else if (e.getSource() == buttonUndo){

			if(arrayListImage.size() <= 1 ){
				buttonUndo.setEnabled(false);
				System.out.println("You cann't do Undo! \n\n");
				tArea.append("You cann't do Undo! \n\n");
			}
			else{
				Image imgRedo = new Image(arrayListImage.remove(arrayListImage.size()-1));
				arrayListRedo.add(imgRedo);
				buttonRedo.setEnabled(true);
				Image img = arrayListImage.get(arrayListImage.size()-1);
				textPane.setText(""); 
				textPane.insertIcon(new ImageIcon(paintImage(img)));
				System.out.println("Undo done! \n\n");
				tArea.append("Undo done! \n\n");
				if(arrayListImage.size() <= 1){
					buttonUndo.setEnabled(false);
				}
			}
		}
		else if (e.getSource() == buttonRedo){
			if(arrayListRedo.size() ==0 ){
				buttonRedo.setEnabled(false);
				System.out.println("You cann't do Redo! \n\n");
				tArea.append("You cann't do Redo! \n\n");
			}
			else{
				Image imgundo = new Image(arrayListRedo.remove(arrayListRedo.size()-1));
				arrayListImage.add(imgundo);
				buttonUndo.setEnabled(true);
				textPane.setText(""); 
				textPane.insertIcon(new ImageIcon(paintImage(imgundo)));
				System.out.println("Redo done! \n\n");
				tArea.append("Redo done! \n\n");
				if(arrayListRedo.size()==0){
					buttonRedo.setEnabled(false);
				}
			}
		}

	}

	public BufferedImage paintImage(Image img){

		int p = 0;
		int a = 255;

		BufferedImage image = new BufferedImage(img.getWidth(), img.getHeight(),  BufferedImage.TYPE_INT_ARGB);

		for(int x = 0; x < img.getWidth(); x++) {
			for(int y = 0; y < img.getHeight(); y++) {
				int r = img.getPixel(x, y).getRed();
				int g = img.getPixel(x, y).getGreen();
				int b = img.getPixel(x, y).getBlue();
				p = (a<<24) | (r<<16) | (g<<8) | b;
				image.setRGB(x, y, p);
			}
		}
		return image;
	}


	public void applyFilter (Filter filter){

		Image img= new Image(arrayListImage.get(arrayListImage.size()-1));
		img = filter.applyFilter(img);

		arrayListImage.add(img);

		textPane.setText(""); 
		textPane.insertIcon(new ImageIcon(paintImage(img)));

		arrayListRedo.clear();

		buttonUndo.setEnabled(true);
		buttonRedo.setEnabled(false);
	}
}
