package ppmImages;

import java.util.Collections;

public class DiffFilter extends NeibFilter {
	
	// It's a neighborhood filter
	// The diff filter calculates the final color component of a pixel
	// as the difference of the minimum from the maximum color value by the corresponding neighboring pixels colors.
	
	public Color neibFilterFunction(){
		
		Collections.sort(redList);
		Collections.sort(greenList);
		Collections.sort(blueList);
		
		int red = (redList.get(redList.size() - 1) - redList.get(0));
		int green = (greenList.get(greenList.size() - 1) - greenList.get(0)); 
		int blue = (blueList.get(blueList.size() - 1) - blueList.get(0));
		Color c = new Color(red, green, blue);
		return c;		
		
	}

}
