package ppmImages;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ColorFilterDialog extends JDialog implements ActionListener, PropertyChangeListener{

	private static final long serialVersionUID = 1L;
	
	private String typedText1 = null;
	private String typedText2 = null;
	private String typedText3 = null;
	private ColorFilterDialogData colorFilterData = new ColorFilterDialogData();

	private JTextField redField;
	private JTextField greenField;
	private JTextField blueField;
	private FilterGUI filterGui;

	private JOptionPane optionPane;

	private String btnOK = "OK";
	private String btnCancel = "Cancel";

	/**
	 * Returns null if the typed string was invalid; otherwise, returns the
	 * string as the user entered it.
	 */
	public String getValidatedText1() {
		return typedText1;
	}
	
	public String getValidatedText2() {
		return typedText2;
	}
	
	public String getValidatedText3() {
		return typedText3;
	}

	public ColorFilterDialogData getDialogColorFilterData() {
		return colorFilterData;
	}


	/** Creates the reusable dialog. */
	public ColorFilterDialog(Frame aFrame, FilterGUI parent) {
		super(aFrame, true);
		filterGui = parent;

		setTitle("Color filter information");

		redField = new JTextField(3);
		greenField = new JTextField(3);
		blueField = new JTextField(3);

		// Create an array of the text and components to be displayed.
		Object[] array = { "Set the Colors \n(The scale is 0 to 255)", "Red color:", redField, "Green color:",
				greenField, "Blue color:", blueField };

		// Create an array specifying the number of dialog buttons
		// and their text.
		Object[] options = { btnOK, btnCancel };

		// Create the JOptionPane.
		optionPane = new JOptionPane(array, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, null, options,
				options[0]);

		// Make this dialog display it.
		setContentPane(optionPane);

		// Handle window closing correctly.
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				/*
				 * Instead of directly closing the window, we're going to change
				 * the JOptionPane's value property.
				 */
				optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
			}
		});

		// Ensure the text field always gets the first focus.
		addComponentListener(new ComponentAdapter() {
			public void componentShown(ComponentEvent ce) {
				redField.requestFocusInWindow();
			}
		});

		// Register an event handler that puts the text into the option pane.
		redField.addActionListener(this);
		greenField.addActionListener(this);
		blueField.addActionListener(this);

		// Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(this);
	}

	/** This method handles events for the text field. */
	public void actionPerformed(ActionEvent e) {
		optionPane.setValue(btnOK);

	}

	/** This method reacts to state changes in the option pane. */
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();

		if (isVisible() && (e.getSource() == optionPane)
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
			Object value = optionPane.getValue();

			if (value == JOptionPane.UNINITIALIZED_VALUE) {
				// ignore reset
				return;
			}

			// Reset the JOptionPane's value.
			// If you don't do this, then if the user
			// presses the same button next time, no
			// property change event will be fired.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (btnOK.equals(value)) {

				colorFilterData.setRedColor(Integer.parseInt(redField.getText()));
				colorFilterData.setGreenColor(Integer.parseInt(greenField.getText()));
				colorFilterData.setBlueColor(Integer.parseInt(blueField.getText()));

				clearAndHide();
			} else { // user closed dialog or clicked cancel
				System.out.println("Canceled.");
				typedText1 = null;
				colorFilterData = null;
				clearAndHide();
			}
		}
	}

	/** This method clears the dialog and hides it. */
	public void clearAndHide() {
		redField.setText(null);
		greenField.setText(null);
		blueField.setText(null);
		setVisible(false);
	}


}
