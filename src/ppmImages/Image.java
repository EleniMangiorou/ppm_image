package ppmImages;

public class Image {


	// Holds the image data 
	Color[] buffer;

	// Width and height of the image (in pixels)
	int width, height, maxColor;

	// Constructors
	public Image() {
		this.width = 0;
		this.height = 0;
		this.buffer = new Color[width * height];
	}

	public Image(int width, int heigth) {
		this.width = width;
		this.height = heigth;
		this.buffer = new Color[width * heigth];
	}

	public Image(int width, int heigth, int maxColor) {
		this.width = width;
		this.height = heigth;
		this.maxColor = maxColor;
		this.buffer = new Color[width * heigth];
	}

	public Image(int width, int heigth, Color[] newBuffer) {
		this.width = width;
		this.height = heigth;
		this.buffer = deepCopy(newBuffer);
	}

	public Image(int width, int height,int maxColor, Color[] newBuffer) {
		this.width = width;
		this.height = height;
		this.maxColor = maxColor;
		this.buffer = deepCopy(newBuffer);
	}

	public Image(Image image) {
		this.width = image.getWidth();
		this.height = image.getHeight();
		this.buffer = deepCopy(image.getBuffer());
	}

	// Returns the width of the image
	public int getWidth() {
		return width;
	}

	// Sets a new value to the width of image
	public void setWidth(int width) {
		if (width >= 0)
			this.width = width;
		else
			System.out.println("This width is wrong (negative value)!!!");
	}

	// Returns the height of the image
	public int getHeight() {
		return height;
	}

	// Sets a new value to the height of image
	public void setHeight(int height) {
		if (height >= 0)
			this.height = height;
		else
			System.out.println("This heigth is wrong (negative value)!!!");

	}

	// Returns the maxColor of the image
	public int getMaxColor() {
		return maxColor;
	}

	// Sets a new value to the maxColor of image
	public void setMaxColor(int maxColor) {

		if ((maxColor > 255) || (maxColor < 0))
			System.out.println("Does not have 8-bits components\n");
		else
			this.maxColor = maxColor;

	}


	// Returns the buffer of the image
	public Color[] getBuffer() {
		return buffer;
	}

	// Gets the color of the image at location (x,y).
	// Does any necessary bound checking.
	// Returns a black (0,0,0) color in case of an out-of-bounds
	// x,y pair
	public Color getPixel(int x, int y) {
		Color color = new Color(0, 0, 0);
		if (x > width || y > height)
			return color;
		else {
			int position = x + y * width;
			color.setColor(buffer[position].getRed(), buffer[position].getGreen(), buffer[position].getBlue());
		}
		return color;
	}

	// Sets the RGB values to a (x,y) pixel.
	public void setPixel(int x, int y, Color value) {
		if (x > width || y > height)
			System.out.println("The (" + x + "," + y + ") is out of the boundaries of the image");
		else {
			int position = x + y * width;
			buffer[position].setRed(value.getRed());
			buffer[position].setGreen(value.getGreen());
			buffer[position].setBlue(value.getBlue());
		}
	}

	// Copies the data to the internal buffer.
	public void setData(Color[] data) {
		this.buffer = data;
	}

	// Changes the size of the internal data to the new one.
	// If the one or both of the dimensions are smaller, 
	// discards the remaining pixels in the rows / columns outside
	// the margins. If the new dimensions are larger, sets the old pixels
	// with zero values (black color).
	public void resize(int newWidth, int newHeigth) {
		Color[] newBuffer = new Color[newWidth * newHeigth];
		if (newWidth < 0) {
			System.out.println("The value of width is negative!!! ");
		} else if (newHeigth < 0) {
			System.out.println("The value of heigth is negative!!! ");
		} else {
			for (int y = 0; y < newHeigth; y++) {
				for (int x = 0; x < newWidth; x++) {
					int position = (x + y * newWidth);
					int old_position = (x + y * width);
					if (y >= height || x >= width) {
						newBuffer[position].setColor(0, 0, 0);
					} else {
						newBuffer[position] = buffer[old_position];
					}
				}
			}
			width = newWidth;
			height = newHeigth;
			buffer = newBuffer;
		}
	}

	// Makes a copy of a Color array
	public Color [] deepCopy(Color [] source){
		Color [] dest = new Color[source.length];
		for (int i = 0; i < dest.length; i++){
			Color c = source[i];
			if (c != null) {
				dest[i] = new Color(c);
			}
		}
		return dest;
	}
	
	public Color AverageValuesofColors(){

		int Redbuffer = 0, Greenbuffer = 0, Bluebuffer = 0;			//Temporary variables for the colors
		int AverageRed = 0, AverageGreen = 0, AverageBlue = 0;	//Temporary variables for the colors, which holds the sum value of each color
		int width = 0, height = 0;			// The width and the height of the image
		Color [] tempbuffer;				// The buffer of the image, which holds the image data
		Color color =  new Color();

		width = getWidth();
		height = getHeight();
		tempbuffer = getBuffer();

		// Calculates the sum value of each color (red, green and blue) from all pixels of the image
		for (int i = 0; i < width* height; i++){
			Redbuffer += tempbuffer[i].getRed();
			Greenbuffer += tempbuffer[i].getGreen();
			Bluebuffer += tempbuffer[i].getBlue();
		}

		// Calculates the average value of the red color of the image 
		AverageRed = (Redbuffer) / (width * height);
		// Calculates the average value of the green color of the image 
		AverageGreen = (Greenbuffer) / (width* height);
		// Calculates the average value of the blue color of the image 
		AverageBlue = (Bluebuffer) / (width * height);

		color.setColor(AverageRed, AverageGreen, AverageBlue);

		return color;

	}

	public String toString(){
		return "\nImage: \nWidth : " + width + "\n\nHeigth : " + height 
				+ "\n\nMaxColor : " + maxColor+ "\n\nBuffer : " + printBuffer();
	}

	// Prints the data of the buffer
	public String printBuffer(){
		String s = new String("");
		for(int i =0; i< this.buffer.length; i++){ 
			s =  s + this.buffer[i].toString();
		}
		return s;
	}

}
