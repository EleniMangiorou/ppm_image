package ppmImages;

import java.io.*;

public class ppm_format {

	// Method for reading a ppm image from a ppm file
	// returns an Image object 
	public Image ReadPPM(String path) throws IOException{

		int redVal = 0, blueVal = 0, greenVal = 0; //Temporary variables for the colors
		
		String imagetype; 	// The type of the image
		int width = 0;		// The width of the image
		int height = 0;		// The height of the image
		int maxColor = 0;	// The max color of the image
		Color [] buffer ;	// The buffer, which holds the image data

		Image image;

		Color color;
		File f = null;

		FileInputStream in = null;
		FileOutputStream out = null;
		f = new File(path);
		in = new FileInputStream(f);

		int c;

		imagetype = readToken(in);
		// Checks if is valid the of image
		if (imagetype.compareTo("P6") != 0 )
			System.out.println("Expected P6 magic number, got " + imagetype);

		width = Integer.parseInt(readToken(in));
		// Checks if is valid the width value
		if (!(width > 0)) 
			System.out.println("Invalid image size (width size)\n");

		height = Integer.parseInt(readToken(in));
		// Checks if is valid the height value
		if (!(height > 0)) 
			System.out.println("Invalid image size (height size)\n");

		maxColor = Integer.parseInt(readToken(in));
		//check valid maxColor value
		if ((maxColor < 0)|| (maxColor > 255)) 
			System.out.println("The image is not 8-bits components\n");

		buffer = new Color [width* height];
		int i = 0;
		// Reads the image data and puts them at the buffer
		while ((c = in.read()) != -1) {
			color =  new Color();

			redVal =  c; 
			greenVal =  in.read();
			blueVal = in.read();

			color.setRed(redVal);
			color.setGreen(greenVal);
			color.setBlue(blueVal);

			buffer[i] = color;
			i++;
		}
		in.close();

		image = new Image(width, height, maxColor, buffer);
		return image;
	}

	// Reads a token of chars, which are delimited by " " or "\n"
	// and returns this token
	public String readToken(FileInputStream input) throws IOException{
		String token = "";
		int c;
		c = input.read();
		while(!((String.valueOf((char)c)).equals(" ")) && !((String.valueOf((char)c)).equals("\n"))){
			token += String.valueOf((char)c);
			c = input.read();
		}
		return token;
	}

	// Method to write an image object into a ppm file
	public boolean WritePPM(Image image, String filename){
		try {
			int  redVal = 0, blueVal = 0, greenVal = 0;	//Temporary variables for the colors

			int maxColor = 255;						// The type of the image
			int width = image.getWidth();			// The width of the image
			int height = image.getHeight();			// The height of the image
			Color [] buffer = image.getBuffer();	// The buffer, which holds the image data

			FileOutputStream out = null;
			File file = new File(filename + ".ppm");

			// Creates the file
			file.createNewFile();

			out = new FileOutputStream(file);
			String imageType = "P6\n" ;
			String strWidth = String.valueOf(width);
			String strHeigth = String.valueOf(height);
			String strMaxColor = String.valueOf(maxColor);
			
			// Writes the type of image (P6) into the file
			out.write(imageType.getBytes());
			// Writes the width of image into the file
			out.write(strWidth.getBytes());
			out.write(("\n").getBytes());
			out.write(strHeigth.getBytes());
			// Writes the height of image into the file
			out.write(("\n").getBytes());
			// Writes the max color of image into the file
			out.write(strMaxColor.getBytes());
			out.write(("\n").getBytes());

			// Writes the image data for the buffer to the file
			for(int i =0; i< width * height; i++){
				redVal = buffer[i].getRed();
				greenVal = buffer[i].getGreen();
				blueVal = buffer[i].getBlue();

				out.write((char)redVal);
				out.write((char)greenVal);
				out.write((char)blueVal);
			}
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}